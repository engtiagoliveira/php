<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Aula 04 - Variáveis</title>
	<link rel="stylesheet" href="../_css/estilo.css">
</head>
<body>
	<div>
		<?php
			$numero = 4; 
			$nome = "Tiago";
			$idade = 26;

			//echo $nome. " tem ". $idade. " anos!"; //ou
			echo "$nome tem $idade anos!"
		?>
	</div>
</body>
</html>