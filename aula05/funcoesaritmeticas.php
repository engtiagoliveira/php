<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Aula 05 - Operadores Aritméticos</title>
	<link rel="stylesheet" href="../_css/estilo.css">
</head>
<body>
	<div>
		<?php
			$v1 = $_GET["x"];
			$v2 = $_GET["y"];

			echo "<h2>Valores recebidos: $v1 e $v2</h2>";
			//abs()
			echo "O valor absoluto de $v2 é: " . abs($v2);

			//pow(base, exp)
			echo "<br> O valor de $v1 <sup>$v2</sup> é: " . pow($v1, $v2);

			//sqrt(arg)
			echo "<br> A raiz de $v1 é: " . sqrt($v1);

			//round(val)
			echo "<br> O valor de $v2 arredondado é: " . round($v2); //ceil(value) sempre arredonda para cima e floor(value) sempre para baixo

			//intval(var)
			echo "<br> A parte inteira de $v2 é: " . intval($v2);

			//number_format(number)
			echo "<br> O valor de $v1 em moeda é: R$" . number_format($v1,2,",",".");
		?>
	</div>
</body>
</html>