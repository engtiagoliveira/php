<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Aula 05 - Operadores Aritméticos</title>
	<link rel="stylesheet" href="../_css/estilo.css">
</head>
<body>
	<div>
		<?php
			$n1 = $_GET["a"];
			$n2 = $_GET["b"];
			$m = ($n1 + $n2)/2;

			echo "A soma vale: ". ($n1+$n2);
			echo "<br> A subtração vale: ". ($n1-$n2);
			echo "<br> A multiplicação vale: ". ($n1*$n2);
			echo "<br> A divisão vale: ".($n1/$n2);
			echo "<br> O módulo vale: ".($n1%$n2);
			echo "<br>A média vale $m";
		?>
	</div>
</body>
</html>