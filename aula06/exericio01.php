<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Aula 05 - Operadores Aritméticos</title>
	<link rel="stylesheet" href="../_css/estilo.css">
</head>
<body>
	<div>
		<?php
			$preco = $_GET["p"];
			echo "O preço do produto é: R$ ". number_format($preco,2,",",".");
			
			// $preco += ($preco*10/100);
			// echo "<br> O novo preço com 10% de aumento será R$ ".number_format($preco,2,",",".");

			$preco -= ($preco*10/100);
			echo "<br> O novo preço com 10% de desconto será R$ ".number_format($preco,2,",",".");

			//pré-incremento e pós incremento

		?>
	</div>
</body>
</html>