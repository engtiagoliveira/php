<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Aula 06 - Operadores de atribuicao</title>
	<link rel="stylesheet" href="../_css/estilo.css">
</head>
<body>
	<div>
		<?php
			//Variáveis referenciadas
			$a = 3;
			$b = &$a; // O & faz a referencia das varáveis.

			$b += 5;

			echo "A variavel A vale: $a";
			echo "<br> A variavel B vale: $b"; 
		?>
	</div>
</body>
</html>