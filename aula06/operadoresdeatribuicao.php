<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Aula 05 - Operadores Aritméticos</title>
	<link rel="stylesheet" href="../_css/estilo.css">
</head>
<body>
	<div>
		<?php
			// $a = 1;
			// $b = 3;
			// //$c = $a + $b;
			// //$c = $c + 5; pode ser substituido por:
			// $c += 5;
			
			// //$b = $b + $a; pode ser substituido por:
			// $b += $a;

			// //$a = $a + 1; ou 
			// $a++;
		?>
	</div>
</body>
</html>